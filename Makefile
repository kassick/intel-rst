IRST_MODULES = intel-rst.o

obj-m += $(IRST_MODULES)

modules: $(KBUILD) $(patsubst %o,%c,$(IRST_MODULES) ) 
	$(MAKE) -C $(KBUILD) M=$(PWD) O=$(KBUILD) modules

clean:
	rm -rf *.o *.ko *.cmd .*.cmd .tmp_versions


#all:
#	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
#
#
#
#clean:
##	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
