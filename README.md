# Rapid Start Technology

Rapid Start Technology is a "new" tecnology that, after suspend, wakes the
bios up after some pre-defined timeout and dumps the memory to some
partition on SSD with the magic id (http://www.intel.com/support/motherboards/desktop/sb/CS-033637.htm ;    http://blog.adios.tw/2012/10/funtoo-linux-and-intel-rapid-start.html ).


# The Driver

The current driver by Matthew Garrett (http://mjg59.dreamwidth.org/26022.html?view=999334&posted=1#cmt999334)
is to be available in kernel 3.11 . For those who can't wait to test it, this is a standalone dkms-ready version of the driver.

# Install

Just run setup.sh and it all should work fine.
I'm a newby in dkms, so don't mind the mess. Corrections and suggestions
are welcome.


# Setup the timer
Edit /etc/default/intel-rst ; change INTEL_RST_TIMEOUT  to a number of
minutes that suit you, run service intel-rst restart
(or just reboot).



