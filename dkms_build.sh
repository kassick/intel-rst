#!/bin/bash
# File: "/home/kassick/tmp/intel-rst/dkms_build.sh"
# Created: "Seg, 22 Jul 2013 00:21:31 +0200 (kassick)"
# Updated: "Seg, 22 Jul 2013 00:31:12 +0200 (kassick)"
# $Id$
# Copyright (C) 2013, Rodrigo Virote Kassick <rvkassick@inf.ufrgs.br> 

. `pwd`/dkms.conf

echo Cmd: sudo dkms build --verbose build ${PACKAGE_NAME}/${PACKAGE_VERSION} -k `uname -r`
sudo dkms build --verbose build ${PACKAGE_NAME}/${PACKAGE_VERSION} -k `uname -r`

echo Cmd: sudo dkms install ${PACKAGE_NAME}/${PACKAGE_VERSION}
sudo dkms install ${PACKAGE_NAME}/${PACKAGE_VERSION}


