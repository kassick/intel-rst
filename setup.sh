#!/bin/sh
# File: "/home/kassick/tmp/intel-rst/setup.sh"
# Created: "Seg, 22 Jul 2013 00:29:23 +0200 (kassick)"
# Updated: "Seg, 22 Jul 2013 00:34:38 +0200 (kassick)"
# $Id$
# Copyright (C) 2013, Rodrigo Virote Kassick <rvkassick@inf.ufrgs.br> 

echo "Setup for DKMS module and scripts for Intel Rapid Start Technology"

echo 

echo "Setup DKMS module"
./dkms_setup.sh

echo "Build first version"
./dkms_build.sh


echo "Install init files"
for f in `find etc -type f `; do
  dest=`dirname $f`
  echo CMd: sudo cp $f /$dest
  sudo cp $f /$dest
done
.
